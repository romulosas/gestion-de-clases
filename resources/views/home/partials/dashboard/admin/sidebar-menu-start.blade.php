@section('sidebar-menu-start')
<ul class="sidebar-menu" id="nav-accordion">
    <li>
        <a class="active" href="index.html">
            <i class="fa fa-dashboard"></i>
            <span>{{ __('Dashboard') }}</span>
        </a>
    </li>

    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-laptop"></i>
            <span>{{ __('Configuraciones') }}</span>
        </a>
        <ul class="sub">
            <li class="sub-menu">
                <a  href="javascript:;">{{ __('Cursos') }}</a>
                <ul class="sub">
                    <li><a  href="{{ route('estadocurso.index') }}">{{ __('Estados de Cursos') }}</a></li>
                </ul>
            </li>

        </ul>
    </li>

    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-book"></i>
            <span>UI Elements</span>
        </a>
        <ul class="sub">
        </ul>
    </li>

    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-cogs"></i>
            <span>Components</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-tasks"></i>
            <span>Form Stuff</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-th"></i>
            <span>Data Tables</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class=" fa fa-envelope"></i>
            <span>Mail</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class=" fa fa-bar-chart-o"></i>
            <span>Charts</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-shopping-cart"></i>
            <span>Shop</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li>
        <a href="google_maps.html" >
            <i class="fa fa-map-marker"></i>
            <span>Google Maps </span>
        </a>
    </li>
    <li class="sub-menu">
        <a href="javascript:;">
            <i class="fa fa-comments-o"></i>
            <span>Chat Room</span>
        </a>
        <ul class="sub">
            <li><a  href="lobby.html">Lobby</a></li>
            <li><a  href="chat_room.html"> Chat Room</a></li>
        </ul>
    </li>
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-glass"></i>
            <span>Extra</span>
        </a>
        <ul class="sub">
        </ul>
    </li>
    <li>
        <a  href="login.html">
            <i class="fa fa-user"></i>
            <span>Login Page</span>
        </a>
    </li>

    <!--multi level menu start-->
    <li class="sub-menu">
        <a href="javascript:;" >
            <i class="fa fa-sitemap"></i>
            <span>Multi level Menu</span>
        </a>
        <ul class="sub">
            <li><a  href="javascript:;">Menu Item 1</a></li>
            <li class="sub-menu">
                <a  href="boxed_page.html">Menu Item 2</a>
                <ul class="sub">
                    <li><a  href="javascript:;">Menu Item 2.1</a></li>
                    <li class="sub-menu">
                        <a  href="javascript:;">Menu Item 3</a>
                        <ul class="sub">
                            <li><a  href="javascript:;">Menu Item 3.1</a></li>
                            <li><a  href="javascript:;">Menu Item 3.2</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <!--multi level menu end-->

</ul>
@endsection
