<?php

use Illuminate\Database\Seeder;

use \App\Models\Admin\Configuracion\CursoEstado;

class CursoEstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = array("Activo", "Terminado", "Finalizado", "Curso Eliminado");
        foreach ($estados as $indice => $nombre){
            factory(CursoEstado::class)->create([
                'name' => $nombre
            ]);
        }
    }
}
