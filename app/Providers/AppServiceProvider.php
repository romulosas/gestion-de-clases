<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $route = app('request')->route();
            if (isset($route)) {
                $action = $route->getAction();
                if (in_array('controller', $action)) {
                    $controller = str_replace("controller", "", strtolower(class_basename($action['controller'])));

                    list($controller, $action) = explode('@', $controller);
                    $view->with(compact('controller', 'action'));
                } else {
                    $view->with(compact('action'));
                }
            }
        });
    }
}
